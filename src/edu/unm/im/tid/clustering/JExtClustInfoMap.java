package edu.unm.im.tid.clustering;
/**
// * Author:	Gergely Zahoranszky-Kohalmi
 * email:	gzahoranszky@gmail.com
 * Class:	JExtractClusteringFromInfoMap.java
 * Version: 1.0.2
 * Date:	04/09/2013
 * 
 */
import java.util.*;
import java.io.*;





public class JExtClustInfoMap {

	/**
	 * @param args
	 */
	
	public String ENDLINE = System.getProperty("line.separator");
	public HashMap<Long,Long> mapping = new HashMap<Long,Long> ();
	public HashMap<Long,String> clustering = new HashMap<Long,String> ();

	private String treeFile = null;
	private String idListFile = null;
	private String outFileName = null;
	private String mapFileName = null;
	private boolean useMappingFileOn = false;
	
	
	public JExtClustInfoMap (String tF, String iF, String oF) {


		setIdListFile (iF);
		registerNodes (iF);
		setTreeFile (tF);

			
		extractClustering (tF, false);
		setOutFileName (oF);


		showClustering ();
		writeClustering ();

	}
	

	public JExtClustInfoMap (String tF, String iF, String oF, String mF) {


		setIdListFile (iF);
		registerNodes (iF);
		setTreeFile (tF);

			
		parseMapping(mF);
		extractClustering (tF, true);
		setOutFileName (oF);

		showClustering ();
		writeClustering ();
	}
	
	
	public String getTreeFile() {
		return treeFile;
	}

	private void setTreeFile(String treeFile) {
		this.treeFile = treeFile;
	}

	public String getIdListFile() {
		return idListFile;
	}

	private void setIdListFile(String idListFile) {
		this.idListFile = idListFile;
	}

	public String getOutFileName() {
		return outFileName;
	}

	private void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}


	public boolean isUseMappingFileOn() {
		return useMappingFileOn;
	}

	private void setUseMappingFileOn(boolean useMappingFileOn) {
		this.useMappingFileOn = useMappingFileOn;
	}

	/**
	 * @return the mapFileName
	 */
	public String getMapFileName() {
		return mapFileName;
	}

	/**
	 * @param mapFileName the mapFileName to set
	 */
	private void setMapFileName(String mapFileName) {
		this.mapFileName = mapFileName;
	}

	public void showClustering() {
		for (Map.Entry <Long,String> entry : clustering.entrySet()) {
			System.out.println(entry.getKey() + "\t" + entry.getValue());
		}
	}

	public void writeClustering() {
		try {
			BufferedWriter fpout = new BufferedWriter(new FileWriter(getOutFileName()));
			for (Map.Entry <Long,String> entry : clustering.entrySet()) {
				fpout.write(entry.getKey() + "\t" + entry.getValue() + ENDLINE);
			}
			fpout.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void registerNodes(String filename) {
		try {
			int initialSingletonIdx = -1; //this is used as default cluster ID. Each registered object is registered initially as a singleton cluster
											//with a subsequently decreasing id number (-1, -2, -3 ...). If the object will be recognized
											//as a cluster member, then this initial singleton number will be replaced to the real cluster ID. 
			BufferedReader fin = new BufferedReader(new FileReader(filename));
			System.out.print("Node list file ** " + filename + " ** succesfully opened." + ENDLINE);
			String line = null;
			while ((line = fin.readLine()) != null)   {
				line = line.trim();
				if (!line.startsWith("#")) {
//					System.out.println (line);
//					Long id = new Long (0);
					if (clustering.keySet().contains(Long.parseLong(line))){
						System.out.println("Duplicate node ID in reference node list.");
						System.exit(10);						
					}
					clustering.put(Long.parseLong(line),Integer.toString(initialSingletonIdx));
					initialSingletonIdx--;
//					System.out.println (Long.parseLong(line));

					//					long objectID = Long.valueOf(temp[0]);
//					String cid = temp[1];
				}

			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void parseMapping (String filename) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(filename));
			System.out.print("InfoMap tree file ** " + filename + " ** succesfully opened." + ENDLINE);
			String line = null;
			while ((line = fin.readLine()) != null)   {
				String[] temp;
				line = line.trim();
				if (!line.startsWith("#")) {
					temp = line.split("\t");
//					System.out.println (temp[0] + " OK " + temp[1]);
					long fromID = Long.parseLong(temp[0]);
					long toID = Long.parseLong(temp[1]);
//					String[] tmp2;
//					tmp2 = temp[1].split("\"");
//					long objectID = Long.parseLong(temp[0]);
//					System.out.println (clusterID + " : " + objectID);
//					for (long Idd clustering.entrySet()){
//						System.out.println(Idd);
//					}

					
					if (!clustering.keySet().contains(toID)){
						System.out.println ("ToID not contained by reference ID list. Terminating..." + ENDLINE);
						System.exit(55);
					}
					else {
						mapping.put(fromID, toID);
					}
				}

			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void extractClustering (String filename, boolean map) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(filename));
			System.out.print("InfoMap tree file ** " + filename + " ** succesfully opened." + ENDLINE);
			String line = null;
			while ((line = fin.readLine()) != null)   {
				String[] temp;
				line = line.trim();
				if (!line.startsWith("#")) {
					temp = line.split(":");
//					System.out.println (temp[0] + " OK " + temp[1]);
					String clusterID = temp[0];
					String[] tmp2;
					tmp2 = temp[1].split("\"");
					long objectID = Long.parseLong(tmp2[1]);
//					System.out.println (clusterID + " : " + objectID);
//					for (long Idd clustering.entrySet()){
//						System.out.println(Idd);
//					}

					
					if (!map){
						if (!clustering.keySet().contains(objectID)){
							System.out.println ("ObjectID not contained by reference ID list. Terminating..." + ENDLINE);
							System.exit(55);
						}
						else {
							clustering.put(objectID, clusterID);
						}
					}
					else {
						objectID = mapping.get(objectID);
						if (!clustering.keySet().contains(objectID)){
							System.out.println ("ObjectID not contained by reference ID list. Terminating..." + ENDLINE);
							System.exit(55);
						}
						else {						
							clustering.put(objectID, clusterID);
						}
					}
				}

			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
