package edu.unm.im.tid.clustering;
/**
 * Author:	Gergely Zahoranszky-Kohalmi
 * email:	gzahoranszky@gmail.com
 * Class:	Clustering.java
 * Version: 1.0.2
 * Date:	04/09/2013
 * 
 * Nested class:	Cluster
 * 
 * Description:
 * 
 * Provides basic functionality for managing clusters, such as creating new cluster, adding members,
 * mapping member IDs to ID matching a reference set, evaluation (sensitivity,specificity).
 * 
 * 
 * 
 */
import java.util.*;
import java.io.*;



public class Clustering {
	// size of reference clusters represented in a cluster)
	public long dataSetSize = 0;
	private String clusteringfile = null;
	public Map<String, Cluster> clusters = new HashMap<String, Cluster> ();
	public Map<Long, Long> mapOfIDs = new HashMap<Long, Long> ();
	public Map <String, Long> clusterSizes = new HashMap <String, Long> ();
	public Map <Long, String> objectID2cid = new HashMap <Long, String> ();
	public Map <Long,List<Long>> neighbors = new HashMap <Long, List<Long>> ();
	String ENDLINE = System.getProperty("line.separator");
	private long nextSingletonCID = -1;
	private long TP = 0;
	private long FP = 0;
	private long TN = 0;
	private long FN = 0;
	
// -- non-essential members --
	private double threshold;
	private String expid;
	private int parallel;

	
	public Clustering ()	{
		setDataSetSize(0);
	}

	public void parseClustering (String filename) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(filename));
			System.out.print("Clustering file ** " + filename + " ** succesfully opened." + ENDLINE);
			String line = null;
			while ((line = fin.readLine()) != null)   {
				String[] temp;
				line = line.trim();
				temp = line.split("\t");
//				System.out.println (temp[0] + " OK " + temp[1]);
				long objectID = Long.parseLong(temp[0]);
				String cid = temp[1];

				if (clusters.get(cid) != null) {
					clusters.get(cid).addMemberToCluster(objectID);
					objectID2cid.put(objectID,cid);
				}
				else {
					Cluster cluster = new Cluster(cid);
					clusters.put(cid, cluster);
					cluster.addMemberToCluster(objectID);
					objectID2cid.put(objectID,cid);
				}
			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void parseClustering (String filename, String mapfile) {
		parseMapping (mapfile);
		try {
			BufferedReader fin = new BufferedReader(new FileReader(filename));
			System.out.print("Clustering file ** " + filename + " ** succesfully opened." + ENDLINE);
			String line = null;;
			while ((line = fin.readLine()) != null)   {
				String[] temp;
				line = line.trim();
				temp = line.split("\t");
				System.out.println (temp[0] + " OK " + temp[1]);
				long objectID = Long.parseLong(temp[0]);
				String cid = temp[1];
				if (clusters.get(cid) != null) {
					clusters.get(cid).addMemberToCluster(mapOfIDs.get(objectID));
					objectID2cid.put(mapOfIDs.get(objectID),cid);					
				}
				else {
					Cluster cluster = new Cluster(cid);
					clusters.put(cid, cluster);
					cluster.addMemberToCluster(mapOfIDs.get(objectID));
					objectID2cid.put(mapOfIDs.get(objectID),cid);					
				}
				
			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateAdjacencyList () {

		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
				Cluster cluster = entry.getValue();
				for (long i : cluster.objectIDs) {
					

					if (Long.parseLong(cluster.getClusterID()) > 0) {


//						System.out.println(Long.parseLong(cluster.getClusterID()));
						List<Long> neighborIDs = new ArrayList<Long> ();
						for (long j : cluster.objectIDs) {
							if (i != j) {
								neighborIDs.add(j);
//								System.out.println ("NS: " + neighborIDs.size());
//								System.out.println (j);
							}
						}
						neighbors.put(i,neighborIDs);
//						System.out.println ("NS: " + neighbors.get(i).size());
						
					}
					// if the singleton cluster, i.e. cid is negative
					else {


						for (long j : cluster.objectIDs) {
							List<Long> neighborIDs = new ArrayList<Long> ();
//							System.out.println ("S: " + neighborIDs.size());
							neighbors.put(i,neighborIDs);		
//							System.out.println ("S: " + neighbors.get(i).size());
						}
					}
				}

		}

/*		for (Map.Entry<Long,List<Long>> entry : neighbors.entrySet()){
			System.out.print(entry.getKey() + ": ");
			List<Long> nl = entry.getValue();
			
			for (long i : nl) {
				System.out.print(i + " ");
			}
			System.out.println("");
		}*/

	}

	

	
	
	public void computeDataSetSize () {
		long sum = 0;

		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
			sum += entry.getValue().getClusterSize();

			if (clusterSizes.get(entry.getKey()) != null) {
				System.out.println("ClusterID duplicate in cluster size distribution computation. Terminating ...");
				System.exit(4);	
			}
			else {
				clusterSizes.put(entry.getKey(), entry.getValue().getClusterSize());
			}
			if (1 == entry.getValue().getClusterSize()) {
				entry.getValue().setSingleton(true);
			}			
		}
		setDataSetSize (sum);
	}
	
	public void showClusterSizeDistribution () {
		for (Map.Entry<String,Long> entry : clusterSizes.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
			//			System.out.println(entry.getKey() + " " + entry.getValue());
		}			
	}	

	

	
	public void parseMapping (String fileName) {
		String line = null;
		long id1 = 0;
		long id2 = 0;
		
		
		try {
			BufferedReader fin = new BufferedReader(new FileReader(fileName));
			while ((line = fin.readLine()) != null)   {
				String[] temp;
				line = line.trim();
				temp = line.split("\t");
//				System.out.println (temp[0] + " OO " + temp[1]);
				id1 = Long.parseLong(temp[0]);
				id2 = Long.parseLong(temp[1]);

				if (mapOfIDs.get(id1) != null) {
					System.out.println("Mapping file contains duplicates. Terminating ...");
					System.exit(3);	
				}
				else {
					mapOfIDs.put(id1,id2);
				}
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Mapping file was not found.");
			e.printStackTrace();
			System.exit(1);			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createNewCluster (String clusterID) {
		Cluster cluster = new Cluster(clusterID);
		clusters.put(clusterID, cluster);
	}
	
	private void addClusterMember (Cluster cluster, int objectID) {
		cluster.addMemberToCluster(objectID);
	}
	
	
	public void showClusters () {

		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
//			System.out.print(entry.getKey() + ": ");
			entry.getValue().showMembers();
			//			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}

	public void makeRefStatForClusters (Map<Long, String> rObjectID2cid) {
		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
			entry.getValue().makeRefStat (rObjectID2cid);
		}
	}
	
	
	
	public void singletonCorrection (Map <Long, String> rID2Clus, Map <String, Long> rClusterSizes) {
		for (Map.Entry<Long, String> e : rID2Clus.entrySet()) {
			if (objectID2cid.get(e.getKey()) == null) {
				Cluster cluster = new Cluster (Long.toString(getNextSingletonCID ()));
				setNextSingletonCID (getNextSingletonCID () - 1);
			}
			
		}
	}
	
	public long getDataSetSize() {
		return dataSetSize;
	}



	private void setDataSetSize(long dataSetSize) {
		this.dataSetSize = dataSetSize;
	}



	private String getClusteringfile() {
		return clusteringfile;
	}



	private void setClusteringfile(String clusteringfile) {
		this.clusteringfile = clusteringfile;
	}


	
	private double getThreshold() {
		return threshold;
	}



	private void setThreshold(double threshold) {
		this.threshold = threshold;
	}



	private String getExpid() {
		return expid;
	}



	private void setExpid(String expid) {
		this.expid = expid;
	}



	private int getParallel() {
		return parallel;
	}



	private void setParallel(int parallel) {
		this.parallel = parallel;
	}


	/**
	 * @return the tP
	 */
	public long getTP() {
		return TP;
	}

	/**
	 * @param tP the tP to set
	 */
	public void setTP(long tP) {
		TP = tP;
	}

	/**
	 * @return the fP
	 */
	public long getFP() {
		return FP;
	}

	/**
	 * @param fP the fP to set
	 */
	public void setFP(long fP) {
		FP = fP;
	}

	/**
	 * @return the tN
	 */
	public long getTN() {
		return TN;
	}

	/**
	 * @param tN the tN to set
	 */
	public void setTN(long tN) {
		TN = tN;
	}

	/**
	 * @return the fN
	 */
	public long getFN() {
		return FN;
	}

	/**
	 * @param fN the fN to set
	 */
	public void setFN(long fN) {
		FN = fN;
	}


	/**
	 * @return the singletonCID
	 */
	public long getNextSingletonCID() {
		return nextSingletonCID;
	}

	/**
	 * @param singletonCID the singletonCID to set
	 */
	public void setNextSingletonCID(long singletonCID) {
		this.nextSingletonCID = singletonCID;
	}


	public void showAdjacencyLists () {
		long resID;
		
		for (Map.Entry<Long, List<Long>> entry: neighbors.entrySet()) {
			resID = entry.getKey();
			List<Long> nIDs = entry.getValue();
			System.out.print (resID + ":");
			for (Long n: nIDs) {
				System.out.print (" " + n);
			}
			System.out.println ("");
		}
	
	}
	



	private class Cluster {
		
		private Map <String, Long> refClustersCount = new HashMap<String, Long>();
		private long clusterSize = 0;
//		private long dataSetSize = 0;
		private String clusterID = null;
		private Set<Long> objectIDs= new HashSet<Long> ();
		private long TP = 0;
		private long FP = 0;
		private long TN = 0;
		private long FN = 0;
		private boolean isSingleton = false; 
		
		private Cluster (String clusterID) {
			setClusterID (clusterID);
		}
	
/*		public void addNewClusterMember (String clusterID) {
			Integer cnt = refClustersCount.get(clusterID);
			refClustersCount.put(clusterID, (cnt == null) ? 1 : cnt + 1);
		}
*/		

		public long getClusterSize() {
			return clusterSize;
		}
		private void setClusterSize(long clusterSize) {
			this.clusterSize = clusterSize;
		}


		private String getClusterID() {
			return clusterID;
		}

		private void setClusterID(String clusterID) {
			this.clusterID = clusterID;
		}
		
		private void addMemberToCluster (long objectID) {
			objectIDs.add(objectID);
			clusterSize++;
		}
		
		private void showMembers () {
			for (long i : objectIDs) {
				System.out.println(Long.toString(i) + "\t" + clusterID);
			}
		}
		
		private void makeRefStat (Map<Long, String> rObjectID2cid) {
			// Creates a mapping between objects IDs and their cluster IDs in the reference clustering.
			String cid;
			for (long i : objectIDs) {
				cid = null;
				cid = rObjectID2cid.get(i);
				Long cnt = refClustersCount.get(cid);
				refClustersCount.put(cid, (cnt == null) ? 1 : cnt + 1);
			}
			showRefClustersCount ();
		}

		
		public void showRefClustersCount () {

			for (Map.Entry<String,Long> entry : refClustersCount.entrySet()) {
//				System.out.print(entry.getKey() + ": ");
				System.out.println (entry.getKey() + " **** " +  entry.getValue());
				//			System.out.println(entry.getKey() + " " + entry.getValue());
			}
		}		
		
	

		/**
		 * @return the tP
		 */
		public long getTP() {	
			return TP;
		}

		/**
		 * @return the fP
		 */
		public long getFP() {
			return FP;
		}

		/**
		 * @return the isSingleton
		 */
		public boolean isSingleton() {
			return isSingleton;
		}

		/**
		 * @param isSingleton the isSingleton to set
		 */
		public void setSingleton(boolean isSingleton) {
			this.isSingleton = isSingleton;
		}

		/**
		 * @return the tN
		 */
		public long getTN() {
			return TN;
		}

		/**
		 * @return the fN
		 */
		public long getFN() {
			return FN;
		}

		
	
		
	}	
}
