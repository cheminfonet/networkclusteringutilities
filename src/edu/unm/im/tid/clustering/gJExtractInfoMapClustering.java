package edu.unm.im.tid.clustering;
//Author:	Gergely Zahoranszky-Kohalmi
//email:	gzahoranszky@gmail.com
//Class:	gJExtractInfoMapClustering.java
//Version: 1.0.2
//Date:	04/09/2013
import java.awt.EventQueue;

import javax.swing.JFrame;

public class gJExtractInfoMapClustering {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gJExtractInfoMapClustering window = new gJExtractInfoMapClustering();
					
					
					window.frame.setSize(450, 620);	
					window.frame.setResizable(false);
					
					ExtractClusteringPanel ecp = new ExtractClusteringPanel ();
					window.frame.getContentPane().add(ecp);
					
					window.frame.setVisible(true);
					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public gJExtractInfoMapClustering() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
