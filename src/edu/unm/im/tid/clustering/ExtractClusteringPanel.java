package edu.unm.im.tid.clustering;
//Author:	Gergely Zahoranszky-Kohalmi
//email:	gzahoranszky@gmail.com
//Class:	ExtractClusteringPanel.java
//Version: 1.0.2
//Date:	04/09/2013
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
//import com.jgoodies.forms.factories.DefaultComponentFactory;

import org.apache.commons.io.*;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;

public class ExtractClusteringPanel extends JPanel {
	private ArrayList<String> InfoMapFiles = new ArrayList<String> ();
	
	private JButton btnBrowse_2;
	private JButton btnBrowse1;
	private JButton btnExtract;
	private JButton btn_IDList;

	private JRadioButton rdbtn_MappingOff;
	private JRadioButton rdbtn_MappingOn;

	private boolean UseMappingFileOn = false;
	private JProgressBar progressBar;

	


	private String NEWLINE = System.getProperty("line.separator");
	private JTextField textField_7;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextArea textArea;

	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
//	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Create the panel.
	 */
	public ExtractClusteringPanel() {
		setBackground(new Color(255, 250, 250));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{151, 77, 101, 85, 82, 0};
		gridBagLayout.rowHeights = new int[]{31, 0, 0, 25, 0, 0, 44, 43, 0, 123, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JMenuBar menuBar = new JMenuBar();
		GridBagConstraints gbc_menuBar = new GridBagConstraints();
		gbc_menuBar.anchor = GridBagConstraints.LINE_START;
		gbc_menuBar.insets = new Insets(5, 0, 5, 0);
		gbc_menuBar.gridwidth = 5;
		gbc_menuBar.fill = GridBagConstraints.BOTH;
		gbc_menuBar.gridx = 0;
		gbc_menuBar.gridy = 0;
		add(menuBar, gbc_menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);
		
/*		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
*/		
		JMenuItem mntmExit = new JMenuItem("Quit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.ALT_MASK));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('h');
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_MASK));
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAbout ();
//				JOptionPane.showMessageDialog(ExtractClusteringPanel.this, "About" + NEWLINE + NEWLINE + "(C) Gergely Zahoranszky-Kohalmi 2012" + NEWLINE + "Email: gzahoranszky@gmail.com" + NEWLINE + NEWLINE + "GUI for JClustEval." + NEWLINE + NEWLINE + "Version 1.0.0." + NEWLINE);

			}
		});
		mnHelp.add(mntmAbout);
		
		JMenuItem mntmInputFields = new JMenuItem("Input");
		mntmInputFields.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.ALT_MASK));
		mntmInputFields.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showInput ();
			}
		});
		mnHelp.add(mntmInputFields);
		
		JMenuItem mntmDescription = new JMenuItem("Description");
		mntmDescription.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.ALT_MASK));
		mntmDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDescription ();
			}
		});
		mnHelp.add(mntmDescription);
		
		JLabel lblEvaluateClustering = new JLabel("Extract InfoMap Clustering");
		lblEvaluateClustering.setForeground(new Color(147, 112, 219));
		lblEvaluateClustering.setFont(new Font("DejaVu Sans", Font.BOLD, 20));
		GridBagConstraints gbc_lblEvaluateClustering = new GridBagConstraints();
		gbc_lblEvaluateClustering.gridwidth = 5;
		gbc_lblEvaluateClustering.insets = new Insets(0, 0, 5, 0);
		gbc_lblEvaluateClustering.gridx = 0;
		gbc_lblEvaluateClustering.gridy = 1;
		add(lblEvaluateClustering, gbc_lblEvaluateClustering);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_1.gridx = 2;
		gbc_rigidArea_1.gridy = 2;
		add(rigidArea_1, gbc_rigidArea_1);
		
	
		
		JLabel lblIdList = new JLabel("Object ID list");
		lblIdList.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblIdList = new GridBagConstraints();
		gbc_lblIdList.anchor = GridBagConstraints.EAST;
		gbc_lblIdList.gridwidth = 2;
		gbc_lblIdList.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdList.gridx = 0;
		gbc_lblIdList.gridy = 3;
		add(lblIdList, gbc_lblIdList);
		
		textField_1 = new JTextField();
		textField_1.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.gridwidth = 2;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 3;
		add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		btn_IDList = new JButton("..");
		btn_IDList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser ();
//				fc.setName("Select mapping file");
				fc.setDialogTitle("gJExtractInfoMapClustering - Select object ID list file");				
				int eventVal = fc.showOpenDialog(ExtractClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_1.setText(selectedFile.getAbsolutePath()); 				
			}
		});
		btn_IDList.setBackground(new Color(255, 255, 255));
		btn_IDList.setFont(new Font("Dialog", Font.BOLD, 10));
		GridBagConstraints gbc_btn_IDList = new GridBagConstraints();
		gbc_btn_IDList.insets = new Insets(0, 0, 5, 0);
		gbc_btn_IDList.gridx = 4;
		gbc_btn_IDList.gridy = 3;
		add(btn_IDList, gbc_btn_IDList);
		
		Component rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_2 = new GridBagConstraints();
		gbc_rigidArea_2.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_2.gridx = 2;
		gbc_rigidArea_2.gridy = 4;
		add(rigidArea_2, gbc_rigidArea_2);
		
		JLabel lblObjectIdMapping = new JLabel("Object ID mapping");
		GridBagConstraints gbc_lblObjectIdMapping = new GridBagConstraints();
		gbc_lblObjectIdMapping.gridwidth = 2;
		gbc_lblObjectIdMapping.anchor = GridBagConstraints.EAST;
		gbc_lblObjectIdMapping.insets = new Insets(0, 0, 5, 5);
		gbc_lblObjectIdMapping.gridx = 0;
		gbc_lblObjectIdMapping.gridy = 5;
		add(lblObjectIdMapping, gbc_lblObjectIdMapping);
		
		rdbtn_MappingOn = new JRadioButton("On");
		rdbtn_MappingOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExtractClusteringPanel.this.setUseMappingFileOn (true);
				textField_2.setEnabled(true);
				btnBrowse1.setEnabled(true);
			}
		});
		
		rdbtn_MappingOff = new JRadioButton("Off");
		rdbtn_MappingOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExtractClusteringPanel.this.setUseMappingFileOn (false);
				textField_2.setEnabled(false);
				btnBrowse1.setEnabled(false);
			}
		});
		buttonGroup_1.add(rdbtn_MappingOff);
		rdbtn_MappingOff.setSelected(true);
		GridBagConstraints gbc_rdbtn_MappingOff = new GridBagConstraints();
		gbc_rdbtn_MappingOff.anchor = GridBagConstraints.WEST;
		gbc_rdbtn_MappingOff.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtn_MappingOff.gridx = 2;
		gbc_rdbtn_MappingOff.gridy = 5;
		add(rdbtn_MappingOff, gbc_rdbtn_MappingOff);
		buttonGroup_1.add(rdbtn_MappingOn);
		GridBagConstraints gbc_rdbtn_MappingOn = new GridBagConstraints();
		gbc_rdbtn_MappingOn.anchor = GridBagConstraints.WEST;
		gbc_rdbtn_MappingOn.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtn_MappingOn.gridx = 3;
		gbc_rdbtn_MappingOn.gridy = 5;
		add(rdbtn_MappingOn, gbc_rdbtn_MappingOn);
		
		JLabel lblMappingFile = new JLabel("Mapping file");
		GridBagConstraints gbc_lblMappingFile = new GridBagConstraints();
		gbc_lblMappingFile.anchor = GridBagConstraints.EAST;
		gbc_lblMappingFile.gridwidth = 2;
		gbc_lblMappingFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblMappingFile.gridx = 0;
		gbc_lblMappingFile.gridy = 6;
		add(lblMappingFile, gbc_lblMappingFile);
		
		textField_2 = new JTextField();
		textField_2.setEnabled(false);
		textField_2.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 2;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 2;
		gbc_textField_2.gridy = 6;
		add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		btnBrowse1 = new JButton("..");
		btnBrowse1.setEnabled(false);
		btnBrowse1.setFont(new Font("Dialog", Font.BOLD, 10));
		btnBrowse1.setBackground(new Color(255, 255, 255));
		btnBrowse1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser ();
//				fc.setName("Select mapping file");
				fc.setDialogTitle("gJExtractInfoMapClustering - Select mapping file");				
				int eventVal = fc.showOpenDialog(ExtractClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_2.setText(selectedFile.getAbsolutePath()); 				
			}
		});
		GridBagConstraints gbc_btnBrowse1 = new GridBagConstraints();
		gbc_btnBrowse1.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse1.gridx = 4;
		gbc_btnBrowse1.gridy = 6;
		add(btnBrowse1, gbc_btnBrowse1);
		
		Component rigidArea_5 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_5 = new GridBagConstraints();
		gbc_rigidArea_5.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_5.gridx = 2;
		gbc_rigidArea_5.gridy = 7;
		add(rigidArea_5, gbc_rigidArea_5);
		
		JLabel lblResultantClusteringFile = new JLabel("InfoMap clustering (.tree)");
		GridBagConstraints gbc_lblResultantClusteringFile = new GridBagConstraints();
		gbc_lblResultantClusteringFile.fill = GridBagConstraints.VERTICAL;
		gbc_lblResultantClusteringFile.anchor = GridBagConstraints.EAST;
		gbc_lblResultantClusteringFile.gridwidth = 2;
		gbc_lblResultantClusteringFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblResultantClusteringFile.gridx = 0;
		gbc_lblResultantClusteringFile.gridy = 8;
		add(lblResultantClusteringFile, gbc_lblResultantClusteringFile);
		
		textField_7 = new JTextField();
		textField_7.setBackground(new Color(230, 230, 250));
		textField_7.setEditable(false);
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.gridwidth = 2;
		gbc_textField_7.insets = new Insets(0, 0, 5, 5);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 2;
		gbc_textField_7.gridy = 8;
		add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		btnBrowse_2 = new JButton("..");
		btnBrowse_2.setFont(new Font("DejaVu Sans", Font.BOLD, 10));
		btnBrowse_2.setForeground(new Color(0, 0, 0));
		btnBrowse_2.setBackground(new Color(255, 255, 255));
		btnBrowse_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser ();
				fc.setDialogTitle("gJExtractInfoMapClustering - Select InfoMap clustering file(s)");				
				fc.setMultiSelectionEnabled(true);
				CluFile cluType = new CluFile ();
				fc.setFileFilter(cluType);
				int eventVal = fc.showOpenDialog(ExtractClusteringPanel.this);
				File selectedFiles []= fc.getSelectedFiles() ;
//				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_7.setText(selectedFile.getName());
				if (eventVal == JFileChooser.APPROVE_OPTION) {
					textArea.setText (null);
					for (File f: selectedFiles) {
						ExtractClusteringPanel.this.addInfoMapFiles (f.getAbsolutePath());
						textArea.append(f.getAbsolutePath() + NEWLINE);
					}
				}

			}
		});
		GridBagConstraints gbc_btnBrowse_2 = new GridBagConstraints();
		gbc_btnBrowse_2.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse_2.gridx = 4;
		gbc_btnBrowse_2.gridy = 8;
		add(btnBrowse_2, gbc_btnBrowse_2);
		btnExtract = new JButton("Extract");
		btnExtract.setForeground(new Color(255, 255, 255));
		btnExtract.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		btnExtract.setBackground(new Color(147, 112, 219));
		btnExtract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validateFields()) {
					btnExtract.setEnabled(false);
					btn_IDList.setEnabled(false);
					textArea.setEnabled(false);
//					textField_3.setEnabled(false);
//					textField_10.setEnabled(false);
					textField_1.setEnabled(false);
					textField_2.setEnabled(false);
					rdbtn_MappingOff.setEnabled(false);
					rdbtn_MappingOn.setEnabled(false);
					btnBrowse1.setEnabled(false);					
					btnBrowse_2.setEnabled(false);					
					
	    			ExtractWorker EW = new ExtractWorker();
	    		    EW.execute();
				}
				else showErrorMessage(); 
// batch mode later				
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 9;
		add(scrollPane, gbc_scrollPane);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		
		Component rigidArea_3 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_3 = new GridBagConstraints();
		gbc_rigidArea_3.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_3.gridx = 2;
		gbc_rigidArea_3.gridy = 10;
		add(rigidArea_3, gbc_rigidArea_3);
		GridBagConstraints gbc_btnExtract = new GridBagConstraints();
		gbc_btnExtract.gridwidth = 5;
		gbc_btnExtract.insets = new Insets(0, 0, 5, 0);
		gbc_btnExtract.gridx = 0;
		gbc_btnExtract.gridy = 11;
		add(btnExtract, gbc_btnExtract);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 2;
		gbc_rigidArea.gridy = 12;
		add(rigidArea, gbc_rigidArea);
		
		JLabel lblStatus = new JLabel("status:");
		lblStatus.setFont(new Font("Dialog", Font.PLAIN, 10));
		GridBagConstraints gbc_lblStatus = new GridBagConstraints();
		gbc_lblStatus.anchor = GridBagConstraints.EAST;
		gbc_lblStatus.insets = new Insets(0, 0, 0, 5);
		gbc_lblStatus.gridx = 0;
		gbc_lblStatus.gridy = 13;
		add(lblStatus, gbc_lblStatus);
		
		progressBar = new JProgressBar();
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_progressBar.gridwidth = 3;
		gbc_progressBar.insets = new Insets(0, 0, 0, 5);
		gbc_progressBar.gridx = 1;
		gbc_progressBar.gridy = 13;
		add(progressBar, gbc_progressBar);
		
	

	}
	
	private void showErrorMessage () {
		JOptionPane.showMessageDialog(ExtractClusteringPanel.this, "Some of the input fields are invalid. Please refer to Help->Input.", "Input error", JOptionPane.ERROR_MESSAGE);
		//.showMessageDialog(ExtractClusteringPanel.this, "Some of the input fields are invalid.");
	}

	private boolean validateFields () {
		
/*		//experiment ID
		if (textField_3.getText().isEmpty()) return false;
		//replicate ID
		if (textField_10.getText().isEmpty()) return false;
		//object ID list 
*/		if (textField_1.getText().isEmpty()) return false;
		//textArea listing the selected InfoMap clustering files 
		if (textArea.getText().isEmpty()) return false;

		
		if (isUseMappingFileOn()) if (textField_2.getText().isEmpty()) return false;
		



		return true;
	}


	public boolean isUseMappingFileOn() {
		return UseMappingFileOn;
	}

	public void setUseMappingFileOn(boolean useMappingFileOn) {
		UseMappingFileOn = useMappingFileOn;
	}

	private void addInfoMapFiles (String newFileName) {
		this.InfoMapFiles.add(newFileName);
	}
	
	private void showInput () {
		String input = "";
		
		input += "Input" + NEWLINE + NEWLINE;
		input += "# <Object ID list>: File containing the list of valid object IDs. One ID per line." + NEWLINE + "ID list must contain all objects to cluster. This is required in order to keep track of singletons." + NEWLINE;
		input += "The list of IDs in <Object ID list> file must be identical to the list of \"newIDs\" in <Mapping file>" + NEWLINE + "if <Object ID mapping> is set to \"On\"" + NEWLINE + NEWLINE;
		input += "# <Object ID mapping>: Optional. If selected a mapping file needs to be defined at <Mapping file>." + NEWLINE + NEWLINE;
		input += "# <Mapping file>: Only required if <Object ID mapping> is set to \"On\"." + NEWLINE + "Line syntax: objectCurrentID<TAB>newID" + NEWLINE + "NewIDs (also referred to as MapIDs) must match the ObjectIDs in the standard clustering file" + NEWLINE + NEWLINE;
		input += "# <InfoMap clustering (.tree)>: A single or more .tree files containing clustering produced by InfoMap algorithm." + NEWLINE + "The version of InfoMap must be identical to the one used in experiments NM_16 and NM_17:" + NEWLINE;
		input += "83bf090d91729e1df1b759dbceacfa49  infomap_undir.tgz" + NEWLINE + NEWLINE;
		input += NEWLINE + "REMARK" + NEWLINE + NEWLINE;
		input += "The set of objectIDs are to be the same in the reference and the produced clustering file." + NEWLINE + "Singletons needs to be listed explicitelyn both files. Singletons are denoted by unique negative IDs." + NEWLINE + "The singleton IDs produced here are in a decreasing manner, but they don't need to be consecutive negative numbers" + NEWLINE +"to be used for gJClusteEval 1.0.1." + NEWLINE + NEWLINE;
		input += "Extracted clustering files(s), i.e. the output will have \".clustering\" extension(s). An output file name is" + NEWLINE + "generated by appending \".clustering\" to the name of the input file." + NEWLINE + NEWLINE;
		input += "Comment lines are not allowed for ID list file and mapping file. Format of .tree file must remain unchanged." + NEWLINE + NEWLINE;
		input += "Order of IDs in <Object ID list> and <Mapping file> can be arbitrary.";
		
		
		JOptionPane.showMessageDialog(ExtractClusteringPanel.this, input);
	}
	
	private void showAbout () {
		String about = "";
		
		about = "About" + NEWLINE + NEWLINE + "(C) Gergely Zahoranszky-Kohalmi 2012-2013" + NEWLINE + "Email: gzahoranszky@gmail.com" + NEWLINE + NEWLINE;
		about += "Translational Informatics Division" + NEWLINE;
		about += "Univeristy of New Mexico School of Medicine" + NEWLINE;
		about += "http://medicine.unm.edu/informatics/index.html" + NEWLINE + NEWLINE;
		about += "GUI for JExtClustInfoMap." + NEWLINE + NEWLINE + "Version 1.0.2.";

		
		JOptionPane.showMessageDialog(ExtractClusteringPanel.this, about);
		
	}

	private void showDescription () {
		String description = "";
		
		description += "Description" + NEWLINE + NEWLINE;
		description += "Extracts clustering from \".tree\" files produced by InfoMap algorithm into tab separated format." + NEWLINE;
		description += "GUI enables for evaluating a number of produced clusterings by selecting multiple InfoMap \".tree\" files." + NEWLINE;
		description += "InfoMap version must be the same used in experiments NM_16 and NM_17:" + NEWLINE + NEWLINE;
		description += "83bf090d91729e1df1b759dbceacfa49  infomap_undir.tgz" + NEWLINE;
		
		JOptionPane.showMessageDialog(ExtractClusteringPanel.this, description);
		
	}	
	
	private class CluFile extends FileFilter {


		@Override
		public boolean accept(File f) {
			// TODO Auto-generated method stub
			
		    if (f.isDirectory()) {
		        return true;
		    }
			
		    String extension = FilenameUtils.getExtension(f.getName());
		    extension = extension.toLowerCase();
		    if (extension != null) {
		        if (extension.equals("tree")) return true;
		    }
		    else
		    	return false;
			
			return false;
		}

		@Override
		public String getDescription() {
			// TODO Auto-generated method stub
			return ".tree";
//			return null;
		}
		
	}
	
	
	
	
	
	private class ExtractWorker extends SwingWorker<Void, String> {

		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			int i = 0;
			if (!InfoMapFiles.isEmpty()) {
				
				//////////////////////
				//1. set up progress bar
				/////////////////////
				progressBar.setMinimum(0);
				progressBar.setValue(0);
				progressBar.setMaximum(InfoMapFiles.size());
				progressBar.setStringPainted(true);	//to display the percentage on the progress bar
													//percentage is computed automatically based on min,
													//mas, and actual value
				
				for (String str: InfoMapFiles) {
					if (!isUseMappingFileOn()) {
						JExtClustInfoMap exC = new JExtClustInfoMap (str, textField_1.getText(), str + ".clustering");
						i++;
						publish(String.valueOf(i));
					}
					else {
						JExtClustInfoMap exC = new JExtClustInfoMap (str, textField_1.getText(), str + ".clustering", textField_2.getText());
						i++;
						publish(String.valueOf(i));
					}
					
				}
			}
				
			JOptionPane.showMessageDialog(ExtractClusteringPanel.this,"Extraction completed.");
			
			InfoMapFiles.clear();
			btnExtract.setEnabled(true);
			textArea.setText(null);
			textField_1.setText(null);
			textField_2.setText(null);
			rdbtn_MappingOff.setSelected(true);
			setUseMappingFileOn(false);
			rdbtn_MappingOn.setSelected(false);

			btn_IDList.setEnabled(true);
			textField_2.setEnabled(false);
			textArea.setEnabled(true);
			textField_1.setEnabled(true);
			rdbtn_MappingOff.setEnabled(true);
			rdbtn_MappingOn.setEnabled(true);
			btnBrowse1.setEnabled(false);					
			btnBrowse_2.setEnabled(true);					
			
			
			
			return null;
		}
		
		@Override
		protected void process (List<String> incoming) {
			for (String str: incoming) {
				String tmp[];
				tmp = str.split("#");
				int ind = 0;
				ind = Integer.parseInt(tmp[0]);
				progressBar.setValue(ind);

			}
		}

		
		
	}
	
	
	
	
	
}
