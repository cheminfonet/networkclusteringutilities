package edu.unm.im.tid.clustering;
//Author:	Gergely Zahoranszky-Kohalmi
//email:	gzahoranszky@gmail.com
//Class:	EvaluateClusteringPanel.java
//Version: 1.0.2
//Date:	04/09/2013
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.JCheckBox;

public class EvaluateClusteringPanel extends JPanel {
	private JTextField textField;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JButton btnBrowse;
	private JButton btnBrowse_2;
	private JButton btnThreshold;
	private JButton btnEvaluate;
	private JCheckBox chckbxOverlapOfClusters;;
	

//	private JButton btnBrowse_4;

	private JRadioButton rdbtnOff;
	private JRadioButton rdbtnOn;
	private boolean BatchModeOn = false;

	private String NEWLINE = System.getProperty("line.separator");
	private String PATHSEP = System.getProperty("file.separator");
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_10;
//	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Create the panel.
	 */
	public EvaluateClusteringPanel() {
		setBackground(new Color(255, 250, 250));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{151, 77, 101, 85, 82, 0};
		gridBagLayout.rowHeights = new int[]{31, 0, 0, 25, 25, 25, 0, 44, 39, 43, 30, 0, 0, 0, 0, 0, 0, 0, 26, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JMenuBar menuBar = new JMenuBar();
		GridBagConstraints gbc_menuBar = new GridBagConstraints();
		gbc_menuBar.anchor = GridBagConstraints.LINE_START;
		gbc_menuBar.insets = new Insets(5, 0, 5, 0);
		gbc_menuBar.gridwidth = 5;
		gbc_menuBar.fill = GridBagConstraints.BOTH;
		gbc_menuBar.gridx = 0;
		gbc_menuBar.gridy = 0;
		add(menuBar, gbc_menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);
		
/*		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
*/		
		JMenuItem mntmExit = new JMenuItem("Quit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.ALT_MASK));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('h');
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_MASK));
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAbout ();
//				JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, "About" + NEWLINE + NEWLINE + "(C) Gergely Zahoranszky-Kohalmi 2012" + NEWLINE + "Email: gzahoranszky@gmail.com" + NEWLINE + NEWLINE + "GUI for JClustEval." + NEWLINE + NEWLINE + "Version 1.0.0." + NEWLINE);

			}
		});
		mnHelp.add(mntmAbout);
		
		JMenuItem mntmInputFields = new JMenuItem("Input");
		mntmInputFields.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.ALT_MASK));
		mntmInputFields.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showInput ();
			}
		});
		mnHelp.add(mntmInputFields);
		
		JMenuItem mntmDescription = new JMenuItem("Description");
		mntmDescription.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.ALT_MASK));
		mntmDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDescription ();
			}
		});
		mnHelp.add(mntmDescription);
		
		JLabel lblEvaluateClustering = new JLabel("Evaluate Clustering");
		lblEvaluateClustering.setForeground(new Color(147, 112, 219));
		lblEvaluateClustering.setFont(new Font("DejaVu Sans", Font.BOLD, 20));
		GridBagConstraints gbc_lblEvaluateClustering = new GridBagConstraints();
		gbc_lblEvaluateClustering.gridwidth = 3;
		gbc_lblEvaluateClustering.insets = new Insets(0, 0, 5, 5);
		gbc_lblEvaluateClustering.gridx = 0;
		gbc_lblEvaluateClustering.gridy = 1;
		add(lblEvaluateClustering, gbc_lblEvaluateClustering);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_1.gridx = 2;
		gbc_rigidArea_1.gridy = 2;
		add(rigidArea_1, gbc_rigidArea_1);
		
		JLabel lblExperimentsId = new JLabel("Experiment's ID");
		GridBagConstraints gbc_lblExperimentsId = new GridBagConstraints();
		gbc_lblExperimentsId.gridwidth = 2;
		gbc_lblExperimentsId.anchor = GridBagConstraints.EAST;
		gbc_lblExperimentsId.insets = new Insets(0, 0, 5, 5);
		gbc_lblExperimentsId.gridx = 0;
		gbc_lblExperimentsId.gridy = 3;
		add(lblExperimentsId, gbc_lblExperimentsId);
		
		textField_3 = new JTextField();
		textField_3.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.gridwidth = 2;
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 2;
		gbc_textField_3.gridy = 3;
		add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);
		
		JLabel lblReplicate = new JLabel("Replicate ID");
		GridBagConstraints gbc_lblReplicate = new GridBagConstraints();
		gbc_lblReplicate.anchor = GridBagConstraints.EAST;
		gbc_lblReplicate.fill = GridBagConstraints.VERTICAL;
		gbc_lblReplicate.gridwidth = 2;
		gbc_lblReplicate.insets = new Insets(0, 0, 5, 5);
		gbc_lblReplicate.gridx = 0;
		gbc_lblReplicate.gridy = 4;
		add(lblReplicate, gbc_lblReplicate);
		
		textField_10 = new JTextField();
		textField_10.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField_10 = new GridBagConstraints();
		gbc_textField_10.insets = new Insets(0, 0, 5, 5);
		gbc_textField_10.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_10.gridx = 2;
		gbc_textField_10.gridy = 4;
		add(textField_10, gbc_textField_10);
		textField_10.setColumns(10);
		
		JLabel lblReferenceClustering = new JLabel("Reference clustering");
		GridBagConstraints gbc_lblReferenceClustering = new GridBagConstraints();
		gbc_lblReferenceClustering.gridwidth = 2;
		gbc_lblReferenceClustering.anchor = GridBagConstraints.EAST;
		gbc_lblReferenceClustering.fill = GridBagConstraints.VERTICAL;
		gbc_lblReferenceClustering.insets = new Insets(0, 0, 5, 5);
		gbc_lblReferenceClustering.gridx = 0;
		gbc_lblReferenceClustering.gridy = 5;
		add(lblReferenceClustering, gbc_lblReferenceClustering);
		
		textField_6 = new JTextField();
		textField_6.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
		textField_6.setForeground(new Color(0, 0, 0));
		textField_6.setBackground(new Color(230, 230, 250));
		textField_6.setEditable(false);
		GridBagConstraints gbc_textField_6 = new GridBagConstraints();
		gbc_textField_6.gridwidth = 2;
		gbc_textField_6.insets = new Insets(0, 0, 5, 5);
		gbc_textField_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_6.gridx = 2;
		gbc_textField_6.gridy = 5;
		add(textField_6, gbc_textField_6);
		textField_6.setColumns(10);
		
		JButton btnBrowse_1 = new JButton("..");
		btnBrowse_1.setFont(new Font("DejaVu Sans", Font.BOLD, 10));
		btnBrowse_1.setBackground(new Color(255, 255, 255));
		btnBrowse_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser ();
				fc.setDialogTitle("gJClustEval - Select reference clustering file");
				int eventVal = fc.showOpenDialog(EvaluateClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
//				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_6.setText(selectedFile.getName()); 				
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_6.setText(selectedFile.getAbsolutePath()); 				

			}
		});
		GridBagConstraints gbc_btnBrowse_1 = new GridBagConstraints();
		gbc_btnBrowse_1.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse_1.gridx = 4;
		gbc_btnBrowse_1.gridy = 5;
		add(btnBrowse_1, gbc_btnBrowse_1);
		
	
		
		Component rigidArea_4 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_4 = new GridBagConstraints();
		gbc_rigidArea_4.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_4.gridx = 2;
		gbc_rigidArea_4.gridy = 6;
		add(rigidArea_4, gbc_rigidArea_4);
		JLabel lblBatchMode = new JLabel("Batch mode");
		GridBagConstraints gbc_lblBatchMode = new GridBagConstraints();
		gbc_lblBatchMode.anchor = GridBagConstraints.EAST;
		gbc_lblBatchMode.insets = new Insets(0, 0, 5, 5);
		gbc_lblBatchMode.gridx = 0;
		gbc_lblBatchMode.gridy = 7;
		add(lblBatchMode, gbc_lblBatchMode);
		
		rdbtnOff = new JRadioButton("Off");
		rdbtnOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBatchModeOn(false);
				textField.setEnabled(false);
				textField_4.setEnabled(false);
				textField_8.setEnabled(true);
				textField_7.setEnabled(true);
				textField_5.setEnabled(false);
				btnBrowse.setEnabled(false);
				btnBrowse_2.setEnabled(true);
				btnThreshold.setEnabled(false);				
			}
		});
		buttonGroup.add(rdbtnOff);
		
				rdbtnOff.setSelected(true);
				GridBagConstraints gbc_rdbtnOff = new GridBagConstraints();
				gbc_rdbtnOff.anchor = GridBagConstraints.WEST;
				gbc_rdbtnOff.insets = new Insets(0, 0, 5, 5);
				gbc_rdbtnOff.gridx = 1;
				gbc_rdbtnOff.gridy = 7;
				add(rdbtnOff, gbc_rdbtnOff);
		
		rdbtnOn = new JRadioButton("On");
		rdbtnOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBatchModeOn(true);
				textField.setEnabled(true);
				textField_4.setEnabled(true);
				textField_8.setEnabled(false);
				textField_7.setEnabled(false);
				textField_5.setEnabled(true);
				btnBrowse.setEnabled(true);	
				btnBrowse_2.setEnabled(false);
				btnThreshold.setEnabled(true);				
			}
		});
		buttonGroup.add(rdbtnOn);
		GridBagConstraints gbc_rdbtnOn = new GridBagConstraints();
		gbc_rdbtnOn.anchor = GridBagConstraints.WEST;
		gbc_rdbtnOn.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnOn.gridx = 2;
		gbc_rdbtnOn.gridy = 7;
		add(rdbtnOn, gbc_rdbtnOn);
		
		chckbxOverlapOfClusters = new JCheckBox("Overlap of clusters enabled");
/*		chckbxOverlapOfClusters.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxOverlapOfClusters.isSelected()) ;
			}
		});*/
		GridBagConstraints gbc_chckbxOverlapOfClusters = new GridBagConstraints();
		gbc_chckbxOverlapOfClusters.gridwidth = 3;
		gbc_chckbxOverlapOfClusters.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxOverlapOfClusters.gridx = 0;
		gbc_chckbxOverlapOfClusters.gridy = 8;
		add(chckbxOverlapOfClusters, gbc_chckbxOverlapOfClusters);
		
		Component rigidArea_5 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_5 = new GridBagConstraints();
		gbc_rigidArea_5.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_5.gridx = 1;
		gbc_rigidArea_5.gridy = 9;
		add(rigidArea_5, gbc_rigidArea_5);
		

		
		JLabel lblSingleFileMode = new JLabel("Single file mode");
		lblSingleFileMode.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
		GridBagConstraints gbc_lblSingleFileMode = new GridBagConstraints();
		gbc_lblSingleFileMode.fill = GridBagConstraints.VERTICAL;
		gbc_lblSingleFileMode.gridwidth = 2;
		gbc_lblSingleFileMode.insets = new Insets(0, 0, 5, 5);
		gbc_lblSingleFileMode.gridx = 2;
		gbc_lblSingleFileMode.gridy = 9;
		add(lblSingleFileMode, gbc_lblSingleFileMode);
		
		JLabel lblResultantClusteringFile = new JLabel("Produced clustering");
		GridBagConstraints gbc_lblResultantClusteringFile = new GridBagConstraints();
		gbc_lblResultantClusteringFile.fill = GridBagConstraints.VERTICAL;
		gbc_lblResultantClusteringFile.anchor = GridBagConstraints.EAST;
		gbc_lblResultantClusteringFile.gridwidth = 2;
		gbc_lblResultantClusteringFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblResultantClusteringFile.gridx = 0;
		gbc_lblResultantClusteringFile.gridy = 10;
		add(lblResultantClusteringFile, gbc_lblResultantClusteringFile);
		
		textField_7 = new JTextField();
		textField_7.setBackground(new Color(230, 230, 250));
		textField_7.setEditable(false);
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.gridwidth = 2;
		gbc_textField_7.insets = new Insets(0, 0, 5, 5);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 2;
		gbc_textField_7.gridy = 10;
		add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		btnBrowse_2 = new JButton("..");
		btnBrowse_2.setFont(new Font("DejaVu Sans", Font.BOLD, 10));
		btnBrowse_2.setForeground(new Color(0, 0, 0));
		btnBrowse_2.setBackground(new Color(255, 255, 255));
		btnBrowse_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser ();
				fc.setDialogTitle("gJClustEval - Select produced clustering file");
				int eventVal = fc.showOpenDialog(EvaluateClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
//				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_7.setText(selectedFile.getName()); 				
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_7.setText(selectedFile.getAbsolutePath()); 				

			}
		});
		GridBagConstraints gbc_btnBrowse_2 = new GridBagConstraints();
		gbc_btnBrowse_2.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse_2.gridx = 4;
		gbc_btnBrowse_2.gridy = 10;
		add(btnBrowse_2, gbc_btnBrowse_2);
		
		JLabel lblThreshold = new JLabel("Threshold");
		GridBagConstraints gbc_lblThreshold = new GridBagConstraints();
		gbc_lblThreshold.gridwidth = 2;
		gbc_lblThreshold.fill = GridBagConstraints.VERTICAL;
		gbc_lblThreshold.anchor = GridBagConstraints.EAST;
		gbc_lblThreshold.insets = new Insets(0, 0, 5, 5);
		gbc_lblThreshold.gridx = 0;
		gbc_lblThreshold.gridy = 11;
		add(lblThreshold, gbc_lblThreshold);
		
		textField_8 = new JTextField();
		textField_8.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.insets = new Insets(0, 0, 5, 5);
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.gridx = 2;
		gbc_textField_8.gridy = 11;
		add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);
		
		Component rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_2 = new GridBagConstraints();
		gbc_rigidArea_2.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_2.gridx = 2;
		gbc_rigidArea_2.gridy = 12;
		add(rigidArea_2, gbc_rigidArea_2);
		
		JLabel lblBatchMode_1 = new JLabel("Batch mode");
		lblBatchMode_1.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
		GridBagConstraints gbc_lblBatchMode_1 = new GridBagConstraints();
		gbc_lblBatchMode_1.gridwidth = 2;
		gbc_lblBatchMode_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblBatchMode_1.gridx = 2;
		gbc_lblBatchMode_1.gridy = 13;
		add(lblBatchMode_1, gbc_lblBatchMode_1);
		
		JLabel lblFolderOfInfomap = new JLabel("Path to folder");
		GridBagConstraints gbc_lblFolderOfInfomap = new GridBagConstraints();
		gbc_lblFolderOfInfomap.gridwidth = 2;
		gbc_lblFolderOfInfomap.anchor = GridBagConstraints.EAST;
		gbc_lblFolderOfInfomap.insets = new Insets(0, 0, 5, 5);
		gbc_lblFolderOfInfomap.gridx = 0;
		gbc_lblFolderOfInfomap.gridy = 14;
		add(lblFolderOfInfomap, gbc_lblFolderOfInfomap);
		
		textField_5 = new JTextField();
		textField_5.setBackground(new Color(230, 230, 250));
		textField_5.setEditable(false);
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.gridwidth = 2;
		gbc_textField_5.insets = new Insets(0, 0, 5, 5);
		gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_5.gridx = 2;
		gbc_textField_5.gridy = 14;
		add(textField_5, gbc_textField_5);
		textField_5.setColumns(10);
		
		btnBrowse = new JButton("..");
		btnBrowse.setFont(new Font("DejaVu Sans", Font.BOLD, 10));
		btnBrowse.setBackground(new Color(255, 255, 255));
		btnBrowse.setEnabled(false);
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser ();
				fc.setDialogTitle("gJClustEval - Select folder of produced clustering files");
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int eventVal = fc.showOpenDialog(EvaluateClusteringPanel.this);
				File selectedDir = fc.getSelectedFile();
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_5.setText(selectedDir.getAbsolutePath()); 
			}
		});
		GridBagConstraints gbc_btnBrowse = new GridBagConstraints();
		gbc_btnBrowse.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse.gridx = 4;
		gbc_btnBrowse.gridy = 14;
		add(btnBrowse, gbc_btnBrowse);
		
		JLabel lblFilenameStem = new JLabel("Clustering name stem");
		lblFilenameStem.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblFilenameStem = new GridBagConstraints();
		gbc_lblFilenameStem.fill = GridBagConstraints.VERTICAL;
		gbc_lblFilenameStem.gridwidth = 2;
		gbc_lblFilenameStem.anchor = GridBagConstraints.EAST;
		gbc_lblFilenameStem.insets = new Insets(0, 0, 5, 5);
		gbc_lblFilenameStem.gridx = 0;
		gbc_lblFilenameStem.gridy = 15;
		add(lblFilenameStem, gbc_lblFilenameStem);
		
		textField_4 = new JTextField();
		textField_4.setBackground(new Color(230, 230, 250));
		textField_4.setEnabled(false);
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.gridwidth = 2;
		gbc_textField_4.insets = new Insets(0, 0, 5, 5);
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.gridx = 2;
		gbc_textField_4.gridy = 15;
		add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIntervalStart = new JLabel("Threshold list");
		GridBagConstraints gbc_lblIntervalStart = new GridBagConstraints();
		gbc_lblIntervalStart.gridwidth = 2;
		gbc_lblIntervalStart.fill = GridBagConstraints.VERTICAL;
		gbc_lblIntervalStart.insets = new Insets(0, 0, 5, 5);
		gbc_lblIntervalStart.anchor = GridBagConstraints.EAST;
		gbc_lblIntervalStart.gridx = 0;
		gbc_lblIntervalStart.gridy = 16;
		add(lblIntervalStart, gbc_lblIntervalStart);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBackground(new Color(230, 230, 250));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 16;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		btnThreshold = new JButton("..");
		btnThreshold.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser ();
				fc.setDialogTitle("gJClustEval - Select threshold list file");				
				int eventVal = fc.showOpenDialog(EvaluateClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
//				if (eventVal == JFileChooser.APPROVE_OPTION)	textField.setText(selectedFile.getName()); 
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField.setText(selectedFile.getAbsolutePath()); 				

			}
		});
		btnThreshold.setBackground(new Color(255, 255, 255));
		btnThreshold.setFont(new Font("DejaVu Sans", Font.BOLD, 10));
		btnThreshold.setEnabled(false);
		GridBagConstraints gbc_btnThreshold = new GridBagConstraints();
		gbc_btnThreshold.insets = new Insets(0, 0, 5, 0);
		gbc_btnThreshold.gridx = 4;
		gbc_btnThreshold.gridy = 16;
		add(btnThreshold, gbc_btnThreshold);

		
		btnEvaluate = new JButton("Evaluate");
		btnEvaluate.setForeground(new Color(255, 255, 255));
		btnEvaluate.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		btnEvaluate.setBackground(new Color(147, 112, 219));
		btnEvaluate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
// single file first
				if (!isBatchModeOn()) {
					if (!validateFields())	showErrorMessage();
					///////////////////////////////////
					//performs SINGLE evaluation here
					///////////////////////////////////
					else {
						JClustEval jCE = new JClustEval (textField_6.getText(), textField_7.getText(), textField_7.getText() + ".eval", textField_3.getText(), textField_10.getText(), textField_8.getText(), chckbxOverlapOfClusters.isSelected());
						JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, "Evaluation done.");
						btnEvaluate.setEnabled(true);
					}
				}
				else {
					if (!validateFields())	showErrorMessage();
					///////////////////////////////////
					//performs BATCH evaluation here
					///////////////////////////////////
					else {
						//using only two decimal places of actual threshold!!!
						
						try {
							BufferedReader br = new BufferedReader(new FileReader (textField.getText()));
							String line = null;
							String stem = "";
							stem = textField_4.getText();
							String path = "";
							path = textField_5.getText();
							String fileToEvaluate = "";
							while ((line = br.readLine()) != null) {
								line = line.trim();
								fileToEvaluate = path + PATHSEP + stem + "." + line + ".clustering";
//								JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, fileToEvaluate);
								JClustEval jCE = new JClustEval (textField_6.getText(), fileToEvaluate, fileToEvaluate + ".eval", textField_3.getText(), textField_10.getText(), line, chckbxOverlapOfClusters.isSelected());
								
							}
							br.close();
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							showErrorMessage ();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							System.exit(0);						}
						
						JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, "Batch evaluation done.");
						btnEvaluate.setEnabled(true);

					}
				}
// batch mode later				
			}
		});
		
		Component rigidArea_3 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_3 = new GridBagConstraints();
		gbc_rigidArea_3.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_3.gridx = 0;
		gbc_rigidArea_3.gridy = 17;
		add(rigidArea_3, gbc_rigidArea_3);
		GridBagConstraints gbc_btnEvaluate = new GridBagConstraints();
		gbc_btnEvaluate.gridwidth = 5;
		gbc_btnEvaluate.insets = new Insets(0, 0, 5, 0);
		gbc_btnEvaluate.gridx = 0;
		gbc_btnEvaluate.gridy = 18;
		add(btnEvaluate, gbc_btnEvaluate);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 2;
		gbc_rigidArea.gridy = 19;
		add(rigidArea, gbc_rigidArea);
		
/*		JLabel lblOuputFile = new JLabel("Ouput file");
		GridBagConstraints gbc_lblOuputFile = new GridBagConstraints();
		gbc_lblOuputFile.fill = GridBagConstraints.VERTICAL;
		gbc_lblOuputFile.anchor = GridBagConstraints.EAST;
		gbc_lblOuputFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblOuputFile.gridx = 0;
		gbc_lblOuputFile.gridy = 25;
		add(lblOuputFile, gbc_lblOuputFile);
		
		textField_9 = new JTextField();
		textField_9.setBackground(new Color(230, 230, 250));
		textField_9.setEditable(false);
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.gridwidth = 2;
		gbc_textField_9.insets = new Insets(0, 0, 5, 5);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 1;
		gbc_textField_9.gridy = 25;
		add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);
		
		JButton btnBrowse_3 = new JButton("Save as..");
		btnBrowse_3.setBackground(new Color(255, 255, 255));
		btnBrowse_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser ();
				int eventVal = fc.showSaveDialog(EvaluateClusteringPanel.this);
				File selectedFile = fc.getSelectedFile();
				if (eventVal == JFileChooser.APPROVE_OPTION)	textField_9.setText(selectedFile.getName()); 					
			}
		});
		GridBagConstraints gbc_btnBrowse_3 = new GridBagConstraints();
		gbc_btnBrowse_3.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowse_3.gridx = 3;
		gbc_btnBrowse_3.gridy = 25;
		add(btnBrowse_3, gbc_btnBrowse_3);
*/		

	}
	
	private void showErrorMessage () {
		JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, "Some of the input fields are invalid. Please refer to Help->Input.", "Input error", JOptionPane.ERROR_MESSAGE);
		//.showMessageDialog(EvaluateClusteringPanel.this, "Some of the input fields are invalid.");
	}

	private boolean validateFields () {
		
		//experiment ID
		if (textField_3.getText().isEmpty()) return false;
		//replicate ID
		if (textField_10.getText().isEmpty()) return false;

		if (textField_6.getText().isEmpty()) return false;
		
		if (!isBatchModeOn()) {
			if (textField_7.getText().isEmpty()) return false;
		
			//Threshold field
			if (textField_8.getText().isEmpty()) return false;
	
			try {
				double i = Double.parseDouble(textField_8.getText());
			}
			catch (NumberFormatException nfe) {
				return false;
			}

		}

		
		//if batch mode is on
		if (isBatchModeOn()) {
			if (textField.getText().isEmpty()) return false;
			if (textField_4.getText().isEmpty()) return false;
			if (textField_5.getText().isEmpty()) return false;




			
		}
		
		

		return true;
	}

	public boolean isBatchModeOn() {
		return BatchModeOn;
	}

	public void setBatchModeOn(boolean batchModeOn) {
		BatchModeOn = batchModeOn;
	}
	

	private void showInput () {
		String input = "";
		
		input += "Input" + NEWLINE + NEWLINE;
		input += "# <Experiment's ID>: aribtrary string" + NEWLINE + NEWLINE;
		input += "# <Replicate ID>: aribtrary string" + NEWLINE + NEWLINE;		
		input += "# <Reference clustering>: File of two columns containing reference clustering," + NEWLINE + "Line syntax: objectID<TAB>clusterID ." + NEWLINE + NEWLINE;		
		input += "# <Batch mode>: if set to \"Off\", only a single clustering file will be evaluated." + NEWLINE + "If set to \"On\", a series of clustering files will be evaulated." + NEWLINE + "The name stem of these files must be the same and the file extensions are to be \".clustering\"." + NEWLINE + NEWLINE;
		input += "# <Produced clustering>: File of two columns containing reference clustering," + NEWLINE + "Line syntax: objectID<TAB>clusterID ." + NEWLINE + NEWLINE;
		input += "# <Threshold>: only required if <Batch mode> is set to \"Off\"." + NEWLINE + "It just provides information for the output file, does not influence the evaluation." + NEWLINE + NEWLINE;		
		input += "# <Path to folder>: only required if <Batch mode> is set to \"On\"." + NEWLINE + "Path to folder containig produced clustering files of extension \".clustering\"." + NEWLINE + NEWLINE;
		input += "# <Clustering name stem>: only required if <Batch mode> is set to \"On\"." + NEWLINE + "Stem of produced clustering file names." + NEWLINE + "Input file name for evaluation is generated by appending " + NEWLINE + "the stem with the actual threshold and \".clustering\". Note, that the actual threshold" + NEWLINE + "is used exactly as it appears in the threshold list file defined in <Threshold list>." + NEWLINE + NEWLINE;
		input += "# <Threshold list>: only required if <Batch mode> is set to \"On\"." + NEWLINE + "File containing one threshold per line." + NEWLINE + NEWLINE;
		input += NEWLINE + "REMARK" + NEWLINE + NEWLINE;
		input += "The set of objectIDs must be the same in the reference and the produced clustering file." + NEWLINE + "Singletons need to be listed explicitely in both files and all singleton must have a unique negative ID." + NEWLINE + "It is not required that the negative singleton IDs are consecutive, which is true for the real cluster IDs as well." + NEWLINE + NEWLINE;
		input += "Evaluation file(s), i.e. the output will have \".eval\" extension(s). An output file name is" + NEWLINE + "generated by appending \".eval\" to the name of the input file." + NEWLINE + NEWLINE;
		input += "Comment lines are not allowed for any type of input." + NEWLINE + NEWLINE;
		input += "Mapping of IDs can only be done at the cluster extraction step." + NEWLINE + NEWLINE;
		input += "Order of IDs reference and produced clustering files can be arbitrary.";		
		
		
		JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, input);
	}
	
	private void showAbout () {
		String about = "";
		
		about = "About" + NEWLINE + NEWLINE + "(C) Gergely Zahoranszky-Kohalmi 2012-2013" + NEWLINE + "Email: gzahoranszky@gmail.com" + NEWLINE + NEWLINE;
		about += "Translational Informatics Division" + NEWLINE;
		about += "Univeristy of New Mexico School of Medicine" + NEWLINE;
		about += "http://medicine.unm.edu/informatics/index.html" + NEWLINE + NEWLINE;		
		about += "GUI for JClustEval." + NEWLINE + NEWLINE + "Version 1.0.2.";
		
		JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, about);
		
	}

	private void showDescription () {
		String description = "";
		
		description += "Description" + NEWLINE + NEWLINE;
		description += "Evaluates produced clustering by comparing the clusters to a reference clustering." + NEWLINE;
		description += "GUI enables for evaluating a number of produced clusterings if using the batch mode." + NEWLINE;
		description += "Evaluation is based on computing sensitivity and specificity." + NEWLINE + NEWLINE;
		description += "sensitivity = TP / (TP + FN)" + NEWLINE;
		description += "specificity = TN / (TN + FP)";
		
		JOptionPane.showMessageDialog(EvaluateClusteringPanel.this, description);
		
	}	
	
}
