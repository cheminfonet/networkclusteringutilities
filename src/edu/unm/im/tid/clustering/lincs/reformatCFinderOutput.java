/*
 * AUTHOR: Gergely Zahoranszky-Kohalmi
 * CONTACT: gzahoranszky@gmail.com
 * 
 * Version: 1.0.2
 * Date:	04/08/2013
 */
package edu.unm.im.tid.clustering.lincs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class reformatCFinderOutput {

	/**
	 * @param args
	 */
	
	private String origDir = null;
	private String newDir = null;
	
	private String NEWLINE = System.getProperty("line.separator");
	private String DIRSEP = System.getProperty("file.separator");
	
	
	private reformatCFinderOutput () {
		;
	}
	
	
	private void replicateCFinderOutput () {
		File currentContent;
		ArrayList<String> dirList = new ArrayList<String> ();



		try {
			FileUtils.copyDirectory(new File(getOrigDir()), new File(getNewDir()));
			
			FileSystemCrawler fC = new FileSystemCrawler (getNewDir());
			
			dirList = fC.getDirectories();
			
			for (String dir: dirList) {
				currentContent = new File (dir);
				
				if (currentContent.isDirectory()) {
					ArrayList<String> filesOfDir = new ArrayList<String>(Arrays.asList(currentContent.list()));
	
					for (String content: filesOfDir) {

						File srcFile = new File (dir + DIRSEP + content);
						
						if (!content.equals(".") && !content.equals("..")) {
							if (srcFile.isFile()) {
	
	
								String destFileName = srcFile + ".bu";
								File destFile = new File (destFileName);
								FileUtils.copyFile(srcFile, destFile);
								FileUtils.forceDelete(srcFile);
	
							}
						}
						
					}
					
				}
			}
			
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.err.println ("ERROR: IO error when copying directory: " + origDir + " to " + newDir + "_cp. Terminating ...");
			System.exit(111);			
		}
		
	}

	private String generateNewFileName (String oldFileName) {
		String newFileName = null;
		
		newFileName = oldFileName.replace(".bu", "");
		
		return newFileName;
		
	}
	
	private void convertFiles () {
		File currentContent;
		ArrayList<String> dirList = new ArrayList<String> ();



		
		FileSystemCrawler fC = new FileSystemCrawler (getNewDir());
		
		dirList = fC.getDirectories();
		
		for (String dir: dirList) {
			currentContent = new File (dir);
			
			if (currentContent.isDirectory()) {
				ArrayList<String> filesOfDir = new ArrayList<String>(Arrays.asList(currentContent.list()));

				for (String content: filesOfDir) {

					File srcFile = new File (dir + DIRSEP + content);
					
					if (srcFile.isFile()) {

						if (!content.equals(".") && !content.equals("..")) {
							//modify file
							System.out.println(dir + DIRSEP + content);
							String origFileName = dir + DIRSEP + content;
							String newFileName = generateNewFileName (origFileName);
							try {
								BufferedReader bR = new BufferedReader (new FileReader (origFileName));
								BufferedWriter bW = new BufferedWriter (new FileWriter (newFileName));
								
								String line = null;
								
								while ((line = bR.readLine()) != null) {
									if (!line.startsWith("#") && line.length() != 0) {
										System.out.println(line);
										bW.write(line + NEWLINE);
									}
								}
								
								bR.close();
								bW.close();
								FileUtils.forceDelete(new File (origFileName));
								
								
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.err.println ("ERROR: original CFinder file " + origFileName + " not found. Terminating ...");
								System.exit (120);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.err.println ("ERROR: IO error at original/new CFinder file " + origFileName + " . Terminating ...");
								System.exit (121);
							}
							
							
						}

					}
					
				}
				
			}
		}
			
			
			

		
	}


	
	private	void reformatOutput () {

		replicateCFinderOutput (); 
		convertFiles ();

/*		
		FileSystemCrawler fC = new FileSystemCrawler (getFileIn());
			
		dirList = fC.getDirectories ();
		
		for (String dir: dirList) {
			System.out.println(dir);
			try {
				FileUtils.copyDirectory(new File(dir), new File(dir + "_cp"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println ("ERROR: IO error when copying directory: " + dir + " to " + dir + "_cp. Terminating ...");
				System.exit(112);
			}
		}
		
		*/
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		CommandLineParser parser = new PosixParser();
		Options options = new Options();
		options.addOption(OptionBuilder.withLongOpt("input")
			.withArgName("directory name")
			.hasArg(true)
			.withDescription("Resultant directory of CFinder 2.0b4 analysis.")
			.isRequired(true)
			.create("i"));
		
		options.addOption("h", "help", false, "show this help");
		
		HelpFormatter helpFormatter = new HelpFormatter();
		
		reformatCFinderOutput rC = new reformatCFinderOutput();
		
		
		
		
		try {
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("i")) rC.setOrigDir(cmd.getOptionValue("i"));
			
			rC.setNewDir (rC.getOrigDir() + "_cp");
			
			rC.reformatOutput ();
			
			
		}
		
		catch(ParseException ex) {
				System.err.println(ex.getMessage());
				helpFormatter.printHelp("java -cp edu.unm.im.tid.lincs.reformatCFinderOutput" + rC.NEWLINE + "Program takes CFinder 2.0b4 output and converts it to a format that is compliant with Delta-SEADOG.", options);
				return;
		} 				
				
		
	}
	
	/**
	 * @return the fileIn
	 */
	public String getOrigDir() {
		return origDir;
	}

	/**
	 * @param fileIn the fileIn to set
	 */
	public void setOrigDir(String oD) {
		this.origDir = oD;
	}


	/**
	 * @return the newDir
	 */
	public String getNewDir() {
		return newDir;
	}


	/**
	 * @param newDir the newDir to set
	 */
	public void setNewDir(String newDir) {
		this.newDir = newDir;
	}
	
	
	
	
/*
	*//**
	 * @return the fileOut
	 *//*
	public String getFileOut() {
		return fileOut;
	}

	*//**
	 * @param fileOut the fileOut to set
	 *//*
	public void setFileOut(String fileOut) {
		this.fileOut = fileOut;
	}

*/
	
	

}
