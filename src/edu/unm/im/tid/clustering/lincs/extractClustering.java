/*
 * AUTHOR: Gergely Zahoranszky-Kohalmi
 * CONTACT: gzahoranszky@gmail.com
 * 
 * version 1.0.2
 * Date: 04/08/2013
 */
package edu.unm.im.tid.clustering.lincs;

import edu.unm.im.tid.clustering.lincs.LInCSClustering;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class extractClustering {

	/**
	 * @param args
	 */
	

	private LInCSClustering clusters;
	private String NEWLINE = System.getProperty("line.separator"); 


	
	public extractClustering (String inputFileName, String outputFileName) {
		;
	}
	
	private extractClustering () {
		;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		extractClustering eC = new extractClustering ();
		
		CommandLineParser parser = new PosixParser();
		Options options = new Options();
		options.addOption(OptionBuilder.withLongOpt("input")
			.withArgName("filename")
			.hasArg(true)
			.withDescription("Resultant file of LInCS analysis. Format: .seadog file.")
			.isRequired(true)
			.create("i"));
		options.addOption(OptionBuilder.withLongOpt("output")
				.withArgName("filename")
				.hasArg(true)
				.withDescription("Name of output file. Format: each line comprised of objectID<TAB>clusterID 2-tuples. No comment will be generated.")
				.isRequired(false)
				.create("o"));
		options.addOption(OptionBuilder.withLongOpt("idlist")
				.withArgName("filename")
				.hasArg(true)
				.withDescription("Name of file containing a full set of original object IDs regardless if object was recognized as a singleton or clustermember. This file helps keep track of objects even if they don't appear in .seadog due to being singletons. Format: each line contains one objectID.")
				.isRequired(true)
				.create("id"));		
		
		options.addOption("h", "help", false, "show this help");
		
		HelpFormatter helpFormatter = new HelpFormatter();	
		try {
			CommandLine cmd = parser.parse(options, args);

			
			
			
			eC.clusters = new LInCSClustering();
			
			if (cmd.hasOption("i")) eC.clusters.setInputFileName(cmd.getOptionValue("i"));
			if (cmd.hasOption("id")) eC.clusters.setIdListFileName(cmd.getOptionValue("id"));
			
			eC.clusters.parseIdList ();
			eC.clusters.parseClustering();
			eC.clusters.showClusters();
			
			if (cmd.hasOption("o")) {
				eC.clusters.setOutputFileName(cmd.getOptionValue("o"));
				eC.clusters.writeClusters();
			}

			//			eC.clusters.showClusterSizeDistribution();
			
			
		}
		
		catch(ParseException ex) {
				System.err.println(ex.getMessage());
				helpFormatter.printHelp("java -cp edu.unm.im.tid.lincs.extractClustering" + eC.NEWLINE + "Note that this program treats .seadog output as a product of non-overlapping clustering. The reason is that this code was written for the purpuse of validating another code and the clustering used for validation produced non-overlapping clusters. Further coding required to deal with overlaps.", options);
				return;
		} 				
		
		
	}
	
	
	

}
