/*
 * 
 * AUTHOR: Gergely Zahoranszky-Kohalmi
 * 
 * CONTACT: gzahoranszky@gmail.com
 * 
 * Class: FileSystemCrawler
 *
 * Version: 1.0.2
 * Date: 04/08/2013
 */
package edu.unm.im.tid.clustering.lincs;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;


public class FileSystemCrawler {

	/**
	 * @param args
	 */
	private String rootName = null;
	private String ID = null;
	private ArrayList<String> dirList = new ArrayList<String> ();
	private ArrayList<String> finalDirList = new ArrayList<String> ();
	private String fileSeparator = System.getProperty("file.separator");


	private boolean followHidden = false;
	
	// Constructor for internal use (using main method of this class)
	////////////////////////////////////////////////////////////////////////
	
	private FileSystemCrawler () {
	}
	

	// Constructor for external use (using from a different class)
	////////////////////////////////////////////////////////////////////////
	public FileSystemCrawler (String root, String ID, boolean fh) {
		setRootName(root);
		setID (ID);
		setFollowHidden (fh);
		
		//////////////////////////////////////////////////////////////////////
		// BREADTH-FIRST-SEARCH Like traverse of directories from given root//
		//////////////////////////////////////////////////////////////////////
		// RECURSION initiation												//
		///////////////////////////////////////////////						//
		String r = getRootName();	
		dirList.add(r);														//
		finalDirList.add(r);												//
		// starts here														//
		///////////////////////////////////////////////						//
		crawlDirectory (r);													//
		// ends here														//
		///////////////////////////////////////////////						//
		//////////////////////////////////////////////////////////////////////

		
		// show traversed directories
		///////////////////////////////////////////////

		
		//		showFinalDirList();
	}
	
	// Constructor for external use (using from a different class)
	////////////////////////////////////////////////////////////////////////
	
	
	public FileSystemCrawler (String root) {
		setRootName(root);
		
		//////////////////////////////////////////////////////////////////////
		// BREADTH-FIRST-SEARCH Like traverse of directories from given root//
		//////////////////////////////////////////////////////////////////////
		// RECURSION initiation												//
		///////////////////////////////////////////////						//
		String r = getRootName();	
		dirList.add(r);														//
		finalDirList.add(r);												//
		// starts here														//
		///////////////////////////////////////////////						//
		crawlDirectory (r);													//
		// ends here														//
		///////////////////////////////////////////////						//
		//////////////////////////////////////////////////////////////////////

		
		// show traversed directories
		///////////////////////////////////////////////

		
		//		showFinalDirList();
	}	
	
	
	

	
	private void setID (String newId) {
		ID = newId;
	}

	private void setRootName (String rName) {
		rootName = rName;
	}
	
	public String getID () {
		return ID;
	}
	
	public String getRootName () {
		return rootName;
	}
	
	private void setFollowHidden (boolean val) {
		followHidden = val;
	}
	

	
	
	
	
	
	/////////////////////////////////////////////////////
	// MAJOR METHOD
	/////////////////////////////////////////////////////
	private void crawlDirectory (String rN) {
		/////////////////////////////////////////////////////////////////////
		// Concept analog to graph breadth-first-search (BFS)
		/////////////////////////////////////////////////////////////////////
		
		File start = new File (rN);	//this must be a directory otherwise error
		String Path = start.getPath() + fileSeparator;

		
//		System.out.println ("Enter: " + start.getPath());
		
		
//		dirList.add(start.getPath() + start.getName());
//		finalDirList.add(start.getPath() + start.getName());
		
		if (!start.isDirectory()) {
			System.err.println("Root is expected to be a directory. Terminating...");
			System.exit(3);
		}
		// Getting content of root and convert it to an arraylist
		// start.list gives the content of the root in format of array: String[]
		// to make it convenient I convert it to an ArrayList on the fly:
		/////////////////////////////////////////////////////////////////////////////

		ArrayList<String> contentOfDir = new ArrayList<String>(Arrays.asList(start.list()));

		for (String s: contentOfDir) {

			File content;	//used for distinguishing files from directories.
			
			// regenerate fuill path
			/////////////////////////////////////////////////////////
			content = new File (Path + s);
			
			
			// Is directory item another directory? At his point we only care about directories. 
			///////////////////////////////////////////////////////////////////////////////////////
			if (content.isDirectory()) {

				
				// DO follow or DO NOT follow hidden folders (starting with a dot) 
				//////////////////////////////////////////////////////////////////////
				if (!followHidden) {
					if (!s.startsWith(".")){
						dirList.add(Path + s);
						finalDirList.add(Path + s);
//						System.out.println ("registered: " + Path + s);
					}
					else ;
				}
				else {
					dirList.add(Path + s);
					finalDirList.add(Path + s);
//					System.out.println ("registered: " + Path + s);
				}
			}
		}

		
		//removing first item on list and starting recursion from item of new index=0
		/////////////////////////////////////////////////////////////////////////////
//		System.out.println("Hello: " + dirList.get(0));
		dirList.remove(0);
		
		
		//recursion starts as long as dirList has at least one item left
		/////////////////////////////////////////////////////////////////////////////
		if (!dirList.isEmpty()){
//			System.out.println ("Out: " + dirList.get(0));
			crawlDirectory (dirList.get(0));
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	public void showFinalDirList () {
		//show items in increasing order as they were registered through directiry traversing
		for (int i=0; i < finalDirList.size(); i++) {
			System.out.println (finalDirList.get(i));
		}
	}
	
	
	// MAIN PRODUCT OF CLASS
	///////////////////////////////////////////////
	public ArrayList<String> getDirectories () {
		return finalDirList; 
	}

}
