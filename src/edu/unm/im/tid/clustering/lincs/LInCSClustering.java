/*
 * AUTHOR: Gergely Zahoranszky-Kohalmi
 * CONTACT: gzahoranszky@gmail.com
 * 
 * version 1.0.2
 * Date: 04/08/2013
 */
package edu.unm.im.tid.clustering.lincs;
/**
 * Author:	Gergely Zahoranszky-Kohalmi
 * email:	gzahoranszky@gmail.com
 * Class:	LInCSClustering.java
 * Version: 1.0.1
 * Date:	04/08/2013
 * 
 * Nested class:	Cluster
 * 
 * Description:
 * 
 * Provides basic functionality for managing clusters, such as creating new cluster, adding members,
 * mapping member IDs to ID matching a reference set, evaluation (sensitivity,specificity).
 * 
 * 
 * 
 */
import java.util.*;
import java.io.*;



public class LInCSClustering {
	// size of reference clusters represented in a cluster)
	public long dataSetSize = 0;
	private String clusteringfile = null;
	public Map<String, Cluster> clusters = new HashMap<String, Cluster> ();
	public Map <String, Long> clusterSizes = new HashMap <String, Long> ();
//	public Map <String, String> objectID2cid = new HashMap <String, String> ();
	private HashMap<String, Boolean> idList = new HashMap<String, Boolean> ();	
//	public Map <Long,List<Long>> neighbors = new HashMap <Long, List<Long>> ();
	private long nextSingletonCID = -1;
	String ENDLINE = System.getProperty("line.separator");
	
	private String inputFileName = null;
	private String outputFileName = null;
	private String idListFileName = null;
	
	public Map<String, Long> clusterNumericIDs = new HashMap <String, Long> ();
	public long clusterNumericID;

	public void registerNewClusterNumericID (String origCID) {
		long cNID = getClusterNumericID () + 1;
		setClusterNumericID (cNID);
		clusterNumericIDs.put(origCID, cNID);
	}

	public long getClusterNumericID () {
		return clusterNumericID;
	}
	
	private void setClusterNumericID (long newID) {
		this.clusterNumericID = newID;
	}
	
	public LInCSClustering ()	{
		setDataSetSize(0);
		setClusterNumericID (0);
	}

	
	
	
	public void parseIdList () {
		String line = null;
		try {
			BufferedReader br = new BufferedReader (new FileReader (getIdListFileName()));
			
			
			while ((line = br.readLine()) != null) {
				line = line.trim();	//it's OK here, no token is expected before/after
				idList.put(line, false);	//record as singleton by default
			}
			
			br.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("ID list file " + getIdListFileName() + " not found. Terminating.");
			System.exit(111);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("I/O error in ID list file " + getIdListFileName() + ". Terminating.");
			System.exit(112);			
		}
		
	}
	

	
	public void parseClustering () {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(getInputFileName()));
			System.out.print("LInCS clustering file ** " + getInputFileName() + " ** succesfully opened." + ENDLINE);
			String line = null;
			String objectID = null;
			String[] clusterIDs;
			boolean startReading = false;
			while ((line = fin.readLine()) != null)   {
				
				if (line.equals("[/Simple Result]")) startReading = false;

				if (startReading) {
					String temp[];
					line = line.trim();
					temp = line.split(":");
	
					objectID = temp[0].trim();
					clusterIDs = temp[1].split(",");
	
					
				
					for (String cid: clusterIDs) {
						cid = cid.trim();
						if (clusterNumericIDs.containsKey(cid)) {
							clusters.get(cid).addMemberToCluster(objectID);
							idList.put (objectID, true);
		//					objectID2cid.put(objectID,clusterID);
						}
						else {
							createNewCluster (cid);
							registerNewClusterNumericID (cid);
							clusters.get(cid).addMemberToCluster(objectID);
							idList.put (objectID, true);
		//					cluster.addMemberToCluster(objectID);
		//					objectID2cid.put(objectID,clusterID);
						}
					}
				
				}

				if (line.equals("[Simple Result]")) startReading = true;
			}			
			
			fin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Clustering file was not found.");
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	
	public void computeDataSetSize () {
		long sum = 0;

		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
			sum += entry.getValue().getClusterSize();

			if (clusterSizes.get(entry.getKey()) != null) {
				System.out.println("ClusterID duplicate in cluster size distribution computation. Terminating ...");
				System.exit(4);	
			}
			else {
				clusterSizes.put(entry.getKey(), entry.getValue().getClusterSize());
			}
			if (1 == entry.getValue().getClusterSize()) {
				entry.getValue().setSingleton(true);
			}			
		}
		setDataSetSize (sum);
	}
	
	public void showClusterSizeDistribution () {
		for (Map.Entry<String,Long> entry : clusterSizes.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
			//			System.out.println(entry.getKey() + " " + entry.getValue());
		}			
	}	

	
	
	private void createNewCluster (String clusterID) {
		Cluster cluster = new Cluster(clusterID);
		clusters.put(clusterID, cluster);
	}
	

	
	
	public void showClusters () {

		for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
//			System.out.print(entry.getKey() + ": ");
			entry.getValue().showMembers();
			//			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		
		//singletons
		////////////////
		for (Map.Entry<String, Boolean> entry: idList.entrySet()) {
			if (!entry.getValue()) {
				System.out.println(entry.getKey() + "\t" + getNextSingletonCID());
				setNextSingletonCID(getNextSingletonCID()-1);
			}
		}		
	}

	
	public void writeClusters () {

		try {
			BufferedWriter bw = new BufferedWriter (new FileWriter (getOutputFileName()));

			for (Map.Entry<String,Cluster> entry : clusters.entrySet()) {
//				System.out.print(entry.getKey() + ": ");
				entry.getValue().writeMembers(bw);
				//			System.out.println(entry.getKey() + " " + entry.getValue());
			}
			
			//singletons
			////////////////
			for (Map.Entry<String, Boolean> entry: idList.entrySet()) {
				if (!entry.getValue()) {
					bw.write(entry.getKey() + "\t" + getNextSingletonCID() + ENDLINE);
					setNextSingletonCID(getNextSingletonCID()-1);
				}
			}
			
			bw.close ();

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("I/O error related to output file " + getOutputFileName());
			System.exit(221);
		}
		

	
	}

	
	public long getDataSetSize() {
		return dataSetSize;
	}



	private void setDataSetSize(long dataSetSize) {
		this.dataSetSize = dataSetSize;
	}



	private String getClusteringfile() {
		return clusteringfile;
	}



	private void setClusteringfile(String clusteringfile) {
		this.clusteringfile = clusteringfile;
	}


	


	/**
	 * @return the singletonCID
	 */
	public long getNextSingletonCID() {
		return nextSingletonCID;
	}

	/**
	 * @param singletonCID the singletonCID to set
	 */
	public void setNextSingletonCID(long singletonCID) {
		this.nextSingletonCID = singletonCID;
	}



	/**
	 * @return the inputFileName
	 */
	public String getInputFileName() {
		return inputFileName;
	}

	/**
	 * @param inputFileName the inputFileName to set
	 */
	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	/**
	 * @return the outputFileName
	 */
	public String getOutputFileName() {
		return outputFileName;
	}

	/**
	 * @param outputFileName the outputFileName to set
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
	
	/**
	 * @return the idListFileName
	 */
	public String getIdListFileName() {
		return idListFileName;
	}

	/**
	 * @param idListFileName the idListFileName to set
	 */
	public void setIdListFileName(String idListFileName) {
		this.idListFileName = idListFileName;
	}





	private class Cluster {
		
		private long clusterSize = 0;
		private String clusterID = null;
		private Set<String> objectIDs= new HashSet<String> ();

		private boolean isSingleton; 
		
		private Cluster (String clusterID) {
			setClusterID (clusterID);
			setSingleton (true);
		}
	
/*		public void addNewClusterMember (String clusterID) {
			Integer cnt = refClustersCount.get(clusterID);
			refClustersCount.put(clusterID, (cnt == null) ? 1 : cnt + 1);
		}
*/		

		public long getClusterSize() {
			return clusterSize;
		}
		private void setClusterSize(long clusterSize) {
			this.clusterSize = clusterSize;
		}


		private String getClusterID() {
			return clusterID;
		}

		private void setClusterID(String clusterID) {
			this.clusterID = clusterID;
		}
		
		private void addMemberToCluster (String objectID) {
			objectIDs.add(objectID);
			clusterSize++;
		}
		
		private void showMembers () {
			for (String i : objectIDs) {
				System.out.println(i + "\t" + clusterID);
			}
		}
		
		
		private void writeMembers (BufferedWriter bw) {
			for (String i : objectIDs) {
				try {
					bw.write(i + "\t" + clusterNumericIDs.get(clusterID) + ENDLINE);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("Error in writing output file " + getOutputFileName());
					System.exit(220);
				}
			}
		}

		/**
		 * @return the isSingleton
		 */
		public boolean isSingleton() {
			return isSingleton;
		}

		/**
		 * @param isSingleton the isSingleton to set
		 */
		public void setSingleton(boolean isSingleton) {
			this.isSingleton = isSingleton;
		}

		
		
		
	}	
}
