package edu.unm.im.tid.clustering;
// Author:	Gergely Zahoranszky-Kohalmi
// email:	gzahoranszky@gmail.com
// Class:	gJClustEval.java
// Version: 1.0.2
// Date:	04/09/2013
import java.awt.EventQueue;


import edu.unm.im.tid.clustering.EvaluateClusteringPanel;

import javax.swing.JFrame;

//import javax.swing.JTabbedPane;


public class gJClustEval {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					gJClustEval window = new gJClustEval();
					
					
					
					window.frame.setSize(400, 600);	
					window.frame.setResizable(false);
					
						
					window.frame.setVisible(true);

					
					
					
//					JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
//					ExtractClusteringPanel ep = new ExtractClusteringPanel ();
//					ep.setVisible(true);
//					tabbedPane.add("Evaluate clustering", ep);

					EvaluateClusteringPanel mp = new EvaluateClusteringPanel ();
					mp.setVisible(true);
//					tabbedPane.add("Extract clustering", mp);
					
//					window.frame.getContentPane().add(tabbedPane);
					window.frame.getContentPane().add(mp);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public gJClustEval() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
